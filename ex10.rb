#a few more of escape sequences to play with print

tabby_cat = "\tI'm tabbed in."
persian_cat = "I'm split\non a line."
backslash_cat = "I'm \\ a \\ cat."

fat_cat = """
I'll do a list:
\t-> Cat food
\t-> Fishies
\t-> Catnip\n\t-> Grass
"""
#fat_rat = '''
#I'll do a list:
#\t-> Cat food
#\t-> Fishies\t-> Catnip\n\t-> Grass '''
puts tabby_cat
puts persian_cat
puts backslash_cat
puts fat_cat
#exercise
puts "See the next line \n\t-> You are seeing tabbed content and there are two genders \n\t* Female \f and \n\t* Male \v"

#trials
puts "trials"
#\\	Backslash ()
puts "\\1tabby_cat"
#\'	Single-quote (')
puts "\'2tabby_cat"
#\"	Double-quote (")
puts "\"3tabby_cat"
#\a	ASCII bell (BEL)
#puts "p12\a4tabby_cat"
#\b	ASCII backspace (BS)
puts "p12'ss\b5tabby_cat"
#\f	ASCII formfeed (FF)
puts "p12\f6tabby_cat"
#\n	ASCII linefeed (LF)
puts "\n'7tabby_cat"
#\r	ASCII Carriage Return (CR)
#puts "p12\r'8tabby_cat"
#\t	ASCII Horizontal Tab (TAB)
puts "\t'9tabby_cat"
#\uxxxx	Character with 16-bit hex value xxxx (Unicode only)
#puts "\10tabby_cat"
#\v	ASCII vertical tab (VT)
puts "p12\v11'tabby_cat"
#\ooo	Character with octal value ooo
puts "p12\ooo12tabby_cat"
#\xhh	Character with hex value hh
#puts '\xhh13"
