#this is study drill file for ex14
user_name,age = ARGV
prompt = '->'

puts "Hi #{user_name}."
puts "you are #{age}yrs old."
puts "I'd like to ask you a few questions."
puts "what is 1+3?"
puts prompt
ans1 = $stdin.gets.chomp

puts "What is 13*4?"
puts prompt
lives = $stdin.gets.chomp

# a comma for puts is like using it twice
puts "What kind of computer do you have? ", prompt
computer = $stdin.gets.chomp

puts """
So, Little trivia for you,
You live in #{lives}.  Not sure where that is.
And you have a #{computer} computer.  Nice.
"""
