#variable declaration is done
types_of_people = 10

#this lesson's learning where a string can hold any number of variable within itself
x = "There are #{types_of_people} types of people."

binary = "binary"

do_not = "don't"

#within single quotes variable declaration doesn't works
y = "Those who know #{binary} and those who #{do_not}."


#here x and y are printed
puts x
puts y

puts "I said: #{x}."
puts "I also said: '#{y}'."

#total of 6 times the variable is used within string and 5 times string within string
#(considering two are directly in puts)

hilarious = true
joke_evaluation = "Isn't that joke so funny?! #{hilarious}"

puts joke_evaluation

w = "This is the left side of..."
e = "a string with a right side."
#string concatenation is used here to print above two variables as one
puts w + e
