input_file = ARGV.first
#print_all function reads all the data in the file.
def print_all(f)
  puts f.read
end
#rewind allows to go the start of file where as previous read function would have taken the pointer to eof.
def rewind(f)
  f.seek(0)
end

def print_a_line(line_count, f)
  puts "#{line_count}, #{f.gets.chomp}"
end
#chomp throws error on empty file.
current_file = open(input_file)

rewind(current_file)
puts "First let's print the whole file:\n"

print_all(current_file)

puts "Now let's rewind, kind of like a tape."

rewind(current_file)

puts "Let's print three lines:"
current_line = 1
print_a_line(current_line, current_file)

current_line += 1
print_a_line(current_line, current_file)

current_line += 1
print_a_line(current_line, current_file)
