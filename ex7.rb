puts "Mary had a little lamb."
puts "Its fleece was white as #{'snow'}."
puts "And everywhere that Mary went."
puts "." *2*2  # what'd that do?
#the above line performs something similar to looping based on the arithmetic operation that is followed

#doubt
loop=2
puts "print "*loop #it works!

end1 = "C"
end2 = "h"
end3 = "e"
end4 = "e"
end5 = "s"
end6 = "e"
end7 = "B"
end8 = "u"
end9 = "r"
end10 = "g"
end11 = "e"
end12 = "r"

# watch that print vs. puts on this line what's it do?
print end1 + end2 + end3 + end4 + end5 + end6 #prints in same lines
puts end7 + end8 + end9 + end10 + end11 + end12 # prints in different line


#print before puts both op in same line and where as puts create a line and new print comes it pushes it to new line
puts "difference"
puts end1 + end2 + end3 + end4 + end5 + end6
print end7 + end8 + end9 + end10 + end11 + end12
