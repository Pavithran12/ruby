days = "Mon Tue Wed Thu Fri Sat Sun"
months = "\nJan\nFeb\nMar\nApr\nMay\nJun\nJul\nAug"
#more printing and more methods to print something with puts in next we use \n
puts "Here are the days: #{days}"
puts "Here are the months: #{months}"
#more printing and more methods to print something with puts in next we use %q its for string values not variables

puts %q{
There's something going on here.
With this weird quote
We'll be able to type as much as we like.
Even 4 lines if we want, or 5, or 6.
#{days}
#{months}
}
