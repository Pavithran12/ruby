from_file, to_file = ARGV

puts "Copying from #{from_file} to #{to_file}"

# we could do these two on one line, how?
in_file = open(from_file)
indata = in_file.read

puts "The input file is #{indata.length} bytes long"

puts "Does the output file exist? #{File.exist?(to_file)}"
puts "Ready, hit RETURN to continue, CTRL-C to abort."
$stdin.gets

out_file = open(to_file, 'w')
out_file.write(indata)

puts "Alright, all done."

out_file.close
in_file.close


# open(var,mode)
#.length gives the length of the File
#.exist? gives boolean o/p
#.write to insert data
#.close to close the file thats been opened


#man cat gives info on the cat function
#echo "This is a test file." > sample17_1.txt
#the above echo line over writes contents of the existing file
#cat sample17_1.txt
#the above line gives the content of the file that you are calling
