formatter = "%{first} %{second} %{third} %{fourth}"
#tryig with #{}
#formatter2 = "#{first} #{second} #{third} #{fourth}" this doesn't work because here above are not direct variables they are more of key

puts formatter % {first: 1, second: 2, third: 3, fourth: 4}
puts formatter % {first: "one", second: "two", third: "three", fourth: "four"}
puts formatter % {first: true, second: false, third: true, fourth: false}
puts formatter % {first: formatter, second: formatter, third: formatter, fourth: formatter}

puts formatter % {
  first: "I had this thing.",
  second: "That you could type up right.",
  third: "But it didn't sing.",
  fourth: "So I said goodnight."
}

# ${} is something similar to #{} where as ${} can be used
#this exercise does the same printing but its string operation more like a key - value pair concept and with keys being printed when value is not assinged
