filename = ARGV.first

puts "We're going to erase #{filename}"
puts "If you don't want that, hit CTRL-C (^C)."
puts "If you do want that, hit RETURN."

$stdin.gets

puts "Opening the file..."
target = open(filename, 'w')
#opening file without  "w" means it opens up in read mode
#https://ruby-doc.org/core-3.1.1/IO.html#method-c-new
#w is write only method which means truncation is done automatically and is not required when opening in write only mode

puts "Truncating the file.  Goodbye!"
target.truncate(0)

puts "Now I'm going to ask you for three lines."

print "line 1: "
line1 = $stdin.gets
print "line 2: "
line2 = $stdin.gets
print "line 3: "
line3 = $stdin.gets
#i have removed chomp in order to put /n in the news inputs
#in output variable have added all the input as one so that no many write methods are called
output= line1+line2+line3

puts "I'm going to write these to the file."

target.write(output)
puts "And finally, we close it."
target.close
