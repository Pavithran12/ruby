#number of cars
cars = 100
#seat number
space_in_a_car = 4.0
#number of drivers
drivers = 30
#number of passengers
passengers = 90
#number of cars with no drivers
cars_not_driven = cars - drivers
#number of cars with drivers
cars_driven = drivers
#all cars have 4 space so that is used to find capacity in 100 cars
carpool_capacity = cars_driven * space_in_a_car
#number of passengers to cars_driven ratio
average_passengers_per_car = passengers / cars_driven


puts "There are #{cars} cars available."
puts "There are only #{drivers} drivers available."
puts "There will be #{cars_not_driven} empty cars today."
puts "We can transport #{carpool_capacity} people today."
puts "We have #{passengers} to carpool today."
puts "We need to put about #{average_passengers_per_car} in each car."

#study drill the error in this file is because of the # symbol in line number 7 or so an error which denotes There
# was no variable declared which is called in line number 14 so the error denotes line number as 14.

# _is used for spaces in variables which means variables cannot have spaces inbetween while assigning or creation
