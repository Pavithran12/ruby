filename = ARGV.first
#this assigns operation to the variable where open is method used to open file
txt = open(filename)
#output line1 with file name
puts "Here's your file #{filename}:"
#this is print with read function over the variable txt
print txt.read
txt.close

print "Type the filename again: "
file_again = $stdin.gets.chomp

#the operation is repeated again
txt_again = open(file_again)

print txt_again.read
txt_again.close
