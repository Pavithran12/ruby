def add(num1, num2)
  puts "add #{num1+num2}"
  puts "sub #{num1-num2}"
  puts "div #{num1/num2}"
  puts "mul #{(num1)*num2}"
end


puts "We can just give the function numbers directly:"
add(20, 30)


puts "OR, we can use variables from our script:"
num1 = 30
num2 = -5

add(num1, num2)


puts "We can even do math inside too:"
add(10 + 20, 5 + 6)


puts "And we can combine the two, variables and math:"
add(((num1/3).to_f), (num2^3))
